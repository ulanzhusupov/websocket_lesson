import 'package:flutter_chat_types/flutter_chat_types.dart' as types;

sealed class MyChatState {}

class ChatInitial extends MyChatState {}

class ChatLoading extends MyChatState {}

class ChatSuccess extends MyChatState {
  final List<types.Message> _messages;

  ChatSuccess({required List<types.Message> messages}) : _messages = messages;
}

class ChatError extends MyChatState {
  final String errorText;

  ChatError({required this.errorText});
}
