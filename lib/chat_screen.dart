import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:websocket_lesson/chat_cubit.dart';
import 'package:websocket_lesson/local_notifications.dart';

class ChatScreen extends StatefulWidget {
  const ChatScreen({super.key});

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  final channel =
      WebSocketChannel.connect(Uri.parse('wss://echo.websocket.org/'));

  final _user = const types.User(
    id: '82091008-a484-4a89-ae75-a22bf8d6f3ac',
    firstName: "Ulan",
    lastName: "Zhusupov",
  );

  final _chanel = const types.User(
    id: '82091008-a484-4a89-ae75-a22bf8d6f3y',
    firstName: "Jolene",
    lastName: "Mkadeaar",
  );

  final List<types.Message> _messages = [];

  @override
  void initState() {
    super.initState();
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.requestNotificationsPermission();

    LocalNotifications.initialize(flutterLocalNotificationsPlugin);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<dynamic>(
          stream: channel.stream,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.active) {
              if (snapshot.hasData) {
                LocalNotifications.showNotification(
                    title: _chanel.firstName.toString(),
                    message: snapshot.data,
                    flutterLocalNotificationsPlugin:
                        flutterLocalNotificationsPlugin);
                _messages.insert(
                  0,
                  types.TextMessage(
                    text: snapshot.data,
                    id: Random().nextInt(10000).toString(),
                    author: types.User(id: _chanel.id),
                  ),
                );

                return SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: Chat(
                    avatarBuilder: (author) => CircleAvatar(
                      radius: 25,
                      backgroundColor: Colors.red,
                      child: Text(author.firstName ?? ""),
                    ),
                    showUserAvatars: true,
                    showUserNames: true,
                    messages: _messages,
                    onSendPressed: (val) {
                      channel.sink.add(val.text);
                      _messages.insert(
                        0,
                        types.TextMessage(
                          author: types.User(id: _user.id),
                          id: Random().nextInt(10000).toString(),
                          text: val.text,
                        ),
                      );
                    },
                    user: _user,
                  ),
                );
              }
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          }),
    );
  }
}
