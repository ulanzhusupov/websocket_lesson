import 'dart:async';
import 'dart:math';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:websocket_lesson/chat_state.dart';

class ChatCubit extends Cubit<MyChatState> {
  final chatStreamController = StreamController<WebSocketChannel>();
  final WebSocketChannel channel =
      WebSocketChannel.connect(Uri.parse('ws://echo.websocket.org/'));

  ChatCubit() : super(ChatInitial());

  void sendMessage(String message, final user, List<types.Message> messages) {
    channel.sink.add(message);

    messages.insert(
      0,
      types.TextMessage(
        text: message,
        id: Random().nextInt(10000).toString(),
        author: types.User(id: user.id),
      ),
    );
    emit(ChatSuccess(messages: messages));
  }

  @override
  Future<void> close() {
    channel.sink.close();
    return super.close();
  }

  Stream<WebSocketChannel> get chatStream => chatStreamController.stream;
}
